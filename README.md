# Gitlab Gui for Windows Tutorial
## Content
  * Install Github Desktop
  * Pull your first Repo from RWTH Git
  * Find url for cloning
  * Find username
  * How to create Rwth-gitlab acces token
  * Additional information

## Install Github Desktop
 Install and download Github Desktop from https://desktop.github.com/
## Pull your first Repo from RWTH Git
* Press the marked button in the top left
  ![](img/github1.png)
* Select Add and Clone repository
  ![](img/github2.png)
  * Select URL and copy the url of your repostory(For more information see "Find url for cloning") in the first textbox and press clone. 
  For example use as cloning url: https://git.rwth-aachen.de/tim_jacobs/gitlab-gui-for-windows-tutorial.git \
  ![](img/github3.PNG)
  * Put in your Gitlab username as Username and your Rwth-gitlab acces token as Password. For more information see "Find username" and "How to create Rwth-gitlab acces token". 
  ![](img/github4.PNG)
  * Now Press Save and retry and your repository is cloned to your Pc. /
  ![](img/github5.PNG)
  * To clone another repository you repeat the first 3 steps. You don't need to login again, your username and token is saved after the first login.


## Find url for cloning
* Login to RWTH git https://git.rwth-aachen.de/
* Select one of your projects or search for another repository.
 ![](img/gitlab1.PNG)
*Press the button clone. Copy the https address.
 ![](img/gitlab2.png)
## Find username
* Login to RWTH git https://git.rwth-aachen.de/
* Open the menu in the top right corner. Your gitlab name is written under your real name after an @. In this case the username is "tim_jacobs".
  ![](img/AccesToken1.png)

## How to create Rwth-gitlab acces token
* Login to RWTH git https://git.rwth-aachen.de/
* Open the menu in the top right corner and select Preferences
  ![](img/AccesToken1.png)
* Select Acces Tokens on the left  
  ![](img/AccesToken2.png)
* Put in Token name, Remove Expiration date, Select all Permissions (Scopes) and press the Create button
 ![](img/AccesToken3.png)
* Copy your token.
 ![](img/AccesToken4.png)

## Additional information
 * The easiest way to create a new Rwth git repostory is to do it in the Rwth GitLab Web IDE and clone it afterwards: https://git.rwth-aachen.de/projects/new
 * Introduction to Github Desktop: https://docs.github.com/en/desktop/installing-and-configuring-github-desktop/overview/getting-started-with-github-desktop
 * General introduction to gitlab: https://docs.gitlab.com/ee/tutorials/




 



 






 



 



 



 






 



 

